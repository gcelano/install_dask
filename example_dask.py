import pathlib
import json
from lxml import etree
import xml.etree.ElementTree as ET
import tqdm
from multiprocessing import get_context
import os

# take only the first match

from dask.distributed import Client, progress

client = Client() # with the ip of the scheduler!

def create_xml(path):
        with open(path) as f:
            file = json.load(f)
            start = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>'
            doctype = '<!DOCTYPE paula SYSTEM "../../paula_feat.dtd">'
            version = '<paula version="1.1">'
            paula_id = "lemma01." + file[0]["paula_id"].replace("tok01_lemma01", "mark_AtticIPA01")
            header = '  <header paula_id="{}"/>'.format(paula_id)
            base = "lemma01." + file[0]["paula_id"].replace("tok01_lemma01", "mark")
            featList = '  <featList xmlns:xlink="http://www.w3.org/1999/xlink" type="AtticIPA01" xml:base="{}">'.format(base + ".xml")
            #print(start, doctype, version,header, featList, sep="\n")
            all_f_el = []
            for d in file:
                    href = d["id"]
                    if d["ipa"]:
                        AtticIPA = d["ipa"][0].replace("5_th_ _BCE_ Attic ipa: ", "")
                    else:
                        AtticIPA = None
                    ff = f'    <feat xlink:href="#{href}" value="{AtticIPA}"/>'
                    lemma = d["value"].replace("-", "- ")
                    comment = f'    <!--{lemma}-->'
                    fea = "\n".join([ff, comment])
                    all_f_el.append(fea)
                    #print(ff, comment, sep="\n")
            fea2 = "\n".join(all_f_el)
            ft = "  </featList>"
            pt = "</paula>"
            xml_doc = "\n".join([start, doctype, version, header, featList, fea2, ft, pt])
            #print(xml_doc)
            #tree = ET.fromstring(xml_doc)
            #final = ET.tostring(tree, encoding="unicode")
            path_w = path.as_posix().split(os.sep)
            file_name = "X3." + path_w[-1].replace("lemma01_ipa01.json", "X3.xml")
            path_w[-1] = file_name
            path_w = os.path.join(*path_w)
            path_w = os.sep + path_w
            #print(path_w)
            with open(path_w, 'w') as f:
                f.write(xml_doc)


path2 = list(pathlib.Path("/nfs_").glob('**/*ipa01.json'))

y = client.map(create_xml, path2, pure=False)

progress(y)

print(set([x.status for x in y]))

print(y)
