* On every computer install (optionally within the virtual environment of Tensorflow):
    * `python -m pip install "dask[complete]"` 
    * `python -m pip install dask distributed --upgrade`
    * choose a computer working as "scheduler" and type at the command line: `dask scheduler` (this prints an ip + port number to use in the following step)
    * choose "worker" machines (which can include the scheduler machine!), and type: `dask worker 192.168.0.236:8786` (the ip is an example and always corresponds to that of the scheduler computer!)
    * if files have to be accessed, they can be made available using python http server: `python -m http.server` which makes the file
    system available at the ip address of the machine, port 8000 (e.g., http://192.168.0.236:8000/)
    * Dask can then be invoked in Ipython:
        ```
        from dask.distributed import Client
        client = Client('127.0.0.1:8786') # this is the ip + port of the scheduler
        ```
    * create a function (e.g `square`) and a list of items to apply to:
        ```
        futures = client.map(square, range(10))
        futures.result()
        ```
    * if a function has to be applied to files, their paths will be `http://192.168.0.236:8000/ ...`, and therefore they will be accessed
    as online resorces (e.g., `urllib.request.urlopen`)
    * see the example `example.py` in this repository

Website: https://distributed.dask.org/en/stable/install.html
